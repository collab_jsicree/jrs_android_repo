package com.example.simpleapp.messages;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.basemessages.M3BaseRequestMessage;

public class RegisterDeviceRequest extends M3BaseRequestMessage {

	// body parameter
	private RegisterDeviceParams body;

	public RegisterDeviceRequest() {
		//call super with body type parameter
		super(RegisterDeviceRequest.class);
	}
	
	public RegisterDeviceParams getBody() {
		return body;
	}

	public void setBody(RegisterDeviceParams body) {
		this.body = body;
	}
	
	
}
