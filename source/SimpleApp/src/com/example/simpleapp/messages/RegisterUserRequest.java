package com.example.simpleapp.messages;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.basemessages.M3BaseRequestMessage;

public class RegisterUserRequest extends M3BaseRequestMessage {

	// body parameter
	private RegisterUserParams body;

	public RegisterUserRequest() {
		//call super with body type parameter
		super(RegisterUserRequest.class);
	}
	
	public RegisterUserParams getBody() {
		return body;
	}

	public void setBody(RegisterUserParams body) {
		this.body = body;
	}
	
	
}
