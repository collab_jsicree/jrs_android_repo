package com.example.simpleapp.messages;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginDeviceParams implements Parcelable {

	private String deviceAppRegistrationTag;
	private String deviceToken;

	public LoginDeviceParams() {
		// TODO Auto-generated constructor stub
	}

	public LoginDeviceParams(String deviceAppRegistrationTag, String deviceToken) {
		super();
		this.deviceAppRegistrationTag = deviceAppRegistrationTag;
		this.deviceToken = deviceToken;
	}

	public String getDeviceAppRegistrationTag() {
		return deviceAppRegistrationTag;
	}

	public void setDeviceAppRegistrationTag(String deviceAppRegistrationTag) {
		this.deviceAppRegistrationTag = deviceAppRegistrationTag;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoginDeviceParams [deviceAppRegistrationTag=");
		builder.append(deviceAppRegistrationTag);
		builder.append(", deviceToken=");
		builder.append(deviceToken);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Implement parcelable interface.
	 */	
	public static final Parcelable.Creator<LoginDeviceParams> CREATOR = new Parcelable.Creator<LoginDeviceParams>() {
		
		public LoginDeviceParams createFromParcel(Parcel pc) {
			LoginDeviceParams registerParams = new LoginDeviceParams();
			
			// populate the instance in the order the fields were written to the parcel
			registerParams.deviceAppRegistrationTag = pc.readString();
			registerParams.deviceToken = pc.readString();
			
			// return the populated instance
			return registerParams;
		}
		
		public LoginDeviceParams[] newArray(int size) {
			return new LoginDeviceParams[size];
		}
	};
	
	@Override
	public int describeContents() {
		// Auto-generated method stub
		return 0;
	}

	/**
	 * write the values to the parcel in the order they should be read.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// write out the fields in the order they should be read
		dest.writeString(this.deviceAppRegistrationTag);
		dest.writeString(this.deviceToken);
	}	
	
}
