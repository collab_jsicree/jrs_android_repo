package com.example.simpleapp.messages;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.basemessages.M3BaseRequestMessage;

public class LoginDeviceRequest extends M3BaseRequestMessage {

	// body parameter
	private LoginDeviceParams body;

	public LoginDeviceRequest() {
		//call super with body type parameter
		super(LoginDeviceRequest.class);
	}
	
	public LoginDeviceParams getBody() {
		return body;
	}

	public void setBody(LoginDeviceParams body) {
		this.body = body;
	}
	
}
