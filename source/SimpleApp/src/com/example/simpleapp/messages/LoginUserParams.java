package com.example.simpleapp.messages;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginUserParams implements Parcelable {

	private String userName;
	private String password;
	private boolean rememberMe;

	public LoginUserParams() {
		// TODO Auto-generated constructor stub
	}

	public LoginUserParams(String userName, String password, boolean rememberMe) {
		super();
		this.userName = userName;
		this.password = password;
		this.rememberMe = rememberMe;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoginUserParams [userName=");
		builder.append(userName);
		builder.append(", password=");
		builder.append(password);
		builder.append(", rememberMe=");
		builder.append(rememberMe);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Implement parcelable interface.
	 */	
	public static final Parcelable.Creator<LoginUserParams> CREATOR = new Parcelable.Creator<LoginUserParams>() {
		
		public LoginUserParams createFromParcel(Parcel pc) {
			LoginUserParams registerParams = new LoginUserParams();
			
			// populate the instance in the order the fields were written to the parcel
			registerParams.userName = pc.readString();
			registerParams.password = pc.readString();
			registerParams.rememberMe = Boolean.getBoolean(pc.readString());
			
			// return the populated instance
			return registerParams;
		}
		
		public LoginUserParams[] newArray(int size) {
			return new LoginUserParams[size];
		}
	};
	
	@Override
	public int describeContents() {
		// Auto-generated method stub
		return 0;
	}

	/**
	 * write the values to the parcel in the order they should be read.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// write out the fields in the order they should be read
		dest.writeString(this.userName);
		dest.writeString(this.password);
		dest.writeString(Boolean.toString(this.rememberMe));
	}	
	
}
