package com.example.simpleapp.messages;

import android.os.Parcel;
import android.os.Parcelable;

public class RegisterUserParams implements Parcelable {

	private String email;
	private String fullName;
	private String password;
	

	public RegisterUserParams() {
		// TODO Auto-generated constructor stub
	}

	

	public RegisterUserParams(String email, String fullName, String password) {
		super();
		this.email = email;
		this.fullName = fullName;
		this.password = password;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getFullName() {
		return fullName;
	}



	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegisterUserParams [email=");
		builder.append(email);
		builder.append(", fullName=");
		builder.append(fullName);
		builder.append(", password=");
		builder.append(password);
		builder.append("]");
		return builder.toString();
	}


	/**
	 * Implement parcelable interface.
	 */	
	public static final Parcelable.Creator<RegisterUserParams> CREATOR = new Parcelable.Creator<RegisterUserParams>() {
		
		public RegisterUserParams createFromParcel(Parcel pc) {
			RegisterUserParams registerParams = new RegisterUserParams();
			
			// populate the instance in the order the fields were written to the parcel
			registerParams.fullName = pc.readString();
			registerParams.email = pc.readString();
			registerParams.password = pc.readString();
			
			// return the populated instance
			return registerParams;
		}
		
		public RegisterUserParams[] newArray(int size) {
			return new RegisterUserParams[size];
		}
	};
	
	@Override
	public int describeContents() {
		// Auto-generated method stub
		return 0;
	}

	/**
	 * write the values to the parcel in the order they should be read.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// write out the fields in the order they should be read
		dest.writeString(this.fullName);
		dest.writeString(this.email);
		dest.writeString(this.password);		
	}
	
	
}
