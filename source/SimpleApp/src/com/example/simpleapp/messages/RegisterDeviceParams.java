package com.example.simpleapp.messages;

import android.os.Parcel;
import android.os.Parcelable;

public class RegisterDeviceParams implements Parcelable {

	private String deviceAppRegistrationTag;

	public RegisterDeviceParams() {
		// TODO Auto-generated constructor stub
	}

	public RegisterDeviceParams(String deviceAppRegistrationTag) {
		super();
		this.deviceAppRegistrationTag = deviceAppRegistrationTag;
	}

	public String getDeviceAppRegistrationTag() {
		return deviceAppRegistrationTag;
	}

	public void setDeviceAppRegistrationTag(String deviceAppRegistrationTag) {
		this.deviceAppRegistrationTag = deviceAppRegistrationTag;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegisterDeviceParams [deviceAppRegistrationTag=");
		builder.append(deviceAppRegistrationTag);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Implement parcelable interface.
	 */	
	public static final Parcelable.Creator<RegisterDeviceParams> CREATOR = new Parcelable.Creator<RegisterDeviceParams>() {
		
		public RegisterDeviceParams createFromParcel(Parcel pc) {
			RegisterDeviceParams registerParams = new RegisterDeviceParams();
			
			// populate the instance in the order the fields were written to the parcel
			registerParams.deviceAppRegistrationTag = pc.readString();
			
			// return the populated instance
			return registerParams;
		}
		
		public RegisterDeviceParams[] newArray(int size) {
			return new RegisterDeviceParams[size];
		}
	};
	
	@Override
	public int describeContents() {
		// Auto-generated method stub
		return 0;
	}

	/**
	 * write the values to the parcel in the order they should be read.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// write out the fields in the order they should be read
		dest.writeString(this.deviceAppRegistrationTag);
	}	
	
}
