package com.example.simpleapp.messages;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.basemessages.M3BaseResponseMessage;
import com.collaborative.m3.clientsdk.serviceapi.servicemessage.messageelements.MessageHeader;


/**
 * POJO containing the functional response message from the HelloWorld service.
 *
 */
public class RegisterUserResponse extends M3BaseResponseMessage {

	public RegisterUserResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegisterUserResponse(MessageHeader m3header) {
		super(m3header);
		// TODO Auto-generated constructor stub
	}
	
}
