package com.example.simpleapp.messages;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.basemessages.M3BaseRequestMessage;

public class LoginUserRequest extends M3BaseRequestMessage {

	// body parameter
	private LoginUserParams body;

	public LoginUserRequest() {
		//call super with body type parameter
		super(LoginUserRequest.class);
	}
	
	public LoginUserParams getBody() {
		return body;
	}

	public void setBody(LoginUserParams body) {
		this.body = body;
	}
	
	
}
