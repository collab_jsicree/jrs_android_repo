package com.example.simpleapp.messages;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.basemessages.M3BaseRequestMessage;

public class BrandRequest extends M3BaseRequestMessage {

	public BrandRequest() {
		//call super with body type parameter
		super(BrandRequest.class);
	}
		
}
