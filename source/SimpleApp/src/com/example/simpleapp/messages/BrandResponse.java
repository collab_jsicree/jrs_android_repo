package com.example.simpleapp.messages;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.basemessages.M3BaseResponseMessage;
import com.collaborative.m3.clientsdk.serviceapi.servicemessage.messageelements.MessageHeader;
import com.example.simpleapp.domain.BrandWrapper;


/**
 * POJO containing the functional response message from the HelloWorld service.
 *
 */
public class BrandResponse extends M3BaseResponseMessage {

	private BrandWrapper body = new BrandWrapper();
	
	public BrandResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BrandResponse(MessageHeader m3header) {
		super(m3header);
		// TODO Auto-generated constructor stub
	}

	public BrandWrapper getBody() {
		return body;
	}

	public void setBody(BrandWrapper body) {
		this.body = body;
	}
	
}
