package com.example.simpleapp.domain;

import java.util.ArrayList;
import java.util.Collection;

import android.os.Parcel;
import android.os.Parcelable;

public class Brands extends ArrayList<Brand> implements Parcelable {

	private static final long serialVersionUID = 1L;

	public Brands() {
		// TODO Auto-generated constructor stub
	}

	public Brands(int capacity) {
		super(capacity);
		// TODO Auto-generated constructor stub
	}

	public Brands(Parcel in) {
		this();
		readFromParcel(in);
	}

	public Brands(Collection<? extends Brand> collection) {
		super(collection);
		// TODO Auto-generated constructor stub
	}

	private void readFromParcel(Parcel in) {
		this.clear();

		// First we have to read the list size
		int size = in.readInt();

		for (int i = 0; i < size; i++) {
			Brand b = new Brand(in.readLong(), in.readString(), in.readString());
			this.add(b);
		}
	}

	public static final Parcelable.Creator<Brands> CREATOR = new Parcelable.Creator<Brands>() {

		public Brands createFromParcel(Parcel pc) {
			return new Brands(pc);
		}

		public Brands[] newArray(int size) {
			return new Brands[size];
		}
		
	};
	@Override
	public int describeContents() {
		// Auto-generated method stub
		return 0;
	}

	/**
	 * write the values to the parcel in the order they should be read.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		int size = this.size();

		// We have to write the list size, we need him recreating the list
		dest.writeInt(size);

		for (int i = 0; i < size; i++) {
			Brand b = this.get(i);

			dest.writeLong(b.getBrandId());
			dest.writeString(b.getName());
			dest.writeString(b.getDescription());
		}
	}		
	
}
