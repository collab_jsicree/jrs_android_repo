package com.example.simpleapp.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class BrandWrapper implements Parcelable {
	protected Brands brands = new Brands();
	
	public BrandWrapper() {
		// TODO Auto-generated constructor stub
	}

	public BrandWrapper(Brands brands) {
		super();
		this.brands = brands;
	}

	public Brands getBrands() {
		return brands;
	}

	public void setBrands(Brands brands) {
		this.brands = brands;
	}

	public static final Parcelable.Creator<BrandWrapper> CREATOR = new Parcelable.Creator<BrandWrapper>() {
		
		public BrandWrapper createFromParcel(Parcel pc) {
			BrandWrapper brands = new BrandWrapper();
			
			// populate the instance in the order the fields were written to the parcel
			brands.brands = pc.readParcelable(null);
			
			// return the populated instance
			return brands;
		}
		
		public BrandWrapper[] newArray(int size) {
			return new BrandWrapper[size];
		}
	};
	
	@Override
	public int describeContents() {
		// Auto-generated method stub
		return 0;
	}

	/**
	 * write the values to the parcel in the order they should be read.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// write out the fields in the order they should be read
		dest.writeParcelable((Parcelable)this.brands, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);		
	}
	

}
