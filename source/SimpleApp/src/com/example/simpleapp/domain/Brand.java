package com.example.simpleapp.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class Brand implements Parcelable {
	protected Long brandId;
	protected String name;
	protected String description;
	
	public Brand() {
		// TODO Auto-generated constructor stub
	}

	public Brand(Long brandId, String name, String description) {
		super();
		this.brandId = brandId;
		this.name = name;
		this.description = description;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Brand [brandId=");
		builder.append(brandId);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}

	public static final Parcelable.Creator<Brand> CREATOR = new Parcelable.Creator<Brand>() {
		
		public Brand createFromParcel(Parcel pc) {
			Brand brand = new Brand();
			
			// populate the instance in the order the fields were written to the parcel
			brand.brandId = pc.readLong();
			brand.name = pc.readString();
			brand.description = pc.readString();
			
			// return the populated instance
			return brand;
		}
		
		public Brand[] newArray(int size) {
			return new Brand[size];
		}
	};
	
	@Override
	public int describeContents() {
		// Auto-generated method stub
		return 0;
	}

	/**
	 * write the values to the parcel in the order they should be read.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// write out the fields in the order they should be read
		dest.writeLong(this.brandId);
		dest.writeString(this.name);
		dest.writeString(this.description);
		
	}
	

}
