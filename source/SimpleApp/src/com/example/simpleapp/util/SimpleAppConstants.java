package com.example.simpleapp.util;

public class SimpleAppConstants {
	
	// TEMPORARY CONSTANT TO HOLD DEVICE TOKEN. USE POSTMAN TO REGISTER THE DEVICE
	// AND THEN COPY THE TOKEN HERE. WHEN YOU REGISTER THE APP, YOUR EMAIL, DEVICE
	// AND RANDOM NUMBER MUST MATCH THE VALUES USED IN THE TAG TO CREATE THE TOKEN.
	public static final String TMP_DEVICE_TOKEN = "";

	/**
	 * Service Constants.
	 */
	public static final String SERVICE_BASE_URL = "https://collab-nexus.azurewebsites.net";
	public static final String SERVICE_DEVICE_LOGIN = "/api/accounts/device/login";
	public static final String SERVICE_USER_LOGIN = "/api/accounts/login";
	public static final String SERVICE_LOGOUT = "/api/accounts/logout";
	public static final String SERVICE_REGISTER_USER = "/api/accounts/register";
	public static final String SERVICE_REGISTER_DEVICE = "/api/accounts/device/register";
	public static final String SERVICE_GET_BRANDS = "/api/brands/list";

	public static final String SERVICE_RESULT = "RESULT";
	public static final String SERVICE_STATUS = "SERVICE_STATUS";
	public static final String SERVICE_STATUS_INFO = "SERVICE_STATUS_DESC";

	/**
	 * Read Only Preferences - Key values 
	 */
	public static final String PREF_NAME_KEY = "pref_name";
	public static final String PREF_EMAIL_KEY = "pref_email";
	public static final String PREF_DEVICE_KEY = "pref_device";
	public static final String PREF_DEVICE_TAG_KEY = "pref_device_tag";
	public static final String PREF_DEVICE_TOKEN_KEY = "pref_device_token";
	
	
	
}
