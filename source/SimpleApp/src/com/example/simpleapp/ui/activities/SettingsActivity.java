package com.example.simpleapp.ui.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.view.Menu;
import android.view.MenuItem;

import com.example.simpleapp.R;
import com.example.simpleapp.ui.fragments.SettingsFragment;
import com.example.simpleapp.util.SimpleAppConstants;

public class SettingsActivity extends Activity implements
		OnSharedPreferenceChangeListener {

	public static final String TAG = SettingsActivity.class.getName();;

	protected SettingsFragment fragment;

	public SettingsActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		if (savedInstanceState == null) {
			fragment = new SettingsFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// // Inflate the menu; this adds items to the action bar if it is
		// present.
		// getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// // Handle action bar item clicks here. The action bar will
		// // automatically handle clicks on the Home/Up button, so long
		// // as you specify a parent activity in AndroidManifest.xml.
		// int id = item.getItemId();
		// if (id == R.id.action_settings) {
		// return true;
		// }
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Set up a listener whenever a key changes
		updatePreference(SimpleAppConstants.PREF_NAME_KEY);
		updatePreference(SimpleAppConstants.PREF_EMAIL_KEY);
		updatePreference(SimpleAppConstants.PREF_DEVICE_KEY);
		updatePreference(SimpleAppConstants.PREF_DEVICE_TAG_KEY);
		updatePreference(SimpleAppConstants.PREF_DEVICE_TOKEN_KEY);
		fragment.getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		// Unregister the listener whenever a key changes
		fragment.getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		updatePreference(key);
	}

	private void updatePreference(String key) {
		if (SimpleAppConstants.PREF_NAME_KEY.equalsIgnoreCase(key)
				|| SimpleAppConstants.PREF_EMAIL_KEY.equalsIgnoreCase(key)
				|| SimpleAppConstants.PREF_DEVICE_KEY.equalsIgnoreCase(key)
				|| SimpleAppConstants.PREF_DEVICE_TAG_KEY.equalsIgnoreCase(key)
				|| SimpleAppConstants.PREF_DEVICE_TOKEN_KEY
						.equalsIgnoreCase(key)) {
			Preference preference = fragment.findPreference(key);
			if (preference != null && preference instanceof EditTextPreference) {
				EditTextPreference editTextPreference = (EditTextPreference) preference;
				if (editTextPreference.getText() != null && editTextPreference.getText().trim().length() > 0) {
					editTextPreference.setSummary(editTextPreference.getText());
				}
			}
		}
	}
}
