package com.example.simpleapp.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.M3MessageConstants;
import com.example.simpleapp.R;
import com.example.simpleapp.domain.BrandWrapper;
import com.example.simpleapp.services.BrandService;
import com.example.simpleapp.ui.fragments.HomeFragment;
import com.example.simpleapp.util.SimpleAppConstants;

public class HomeActivity extends BaseActivity {

	public static final String TAG = HomeActivity.class.getName();

	// protected HomeFragment fragment;

	public HomeActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		if (savedInstanceState == null) {
			fragment = new HomeFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			showSettings();
		} else if (id == R.id.action_register) {
			showRegistration();
		}
		return super.onOptionsItemSelected(item);
	}

	protected void showSettings() {
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);
	}

	protected void showRegistration() {
		Intent intent = new Intent(this, RegisterActivity.class);
		startActivity(intent);
	}

	public void showBrands(View view) {
		Log.d(TAG, "In showBrands()");

		Intent intent = new Intent(HomeActivity.this, BrandService.class);
		intent.setAction(BrandService.ACTION_GET_BRANDS);
		startService(intent);

	}

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				onReceiveBroadcast);

	}

	@Override
	protected void onResume() {
		super.onResume();

		// create the intent filter
		IntentFilter filter = new IntentFilter();
		filter.addAction(BrandService.ACTION_FIND_BRAND);
		filter.addAction(BrandService.ACTION_GET_BRANDS);

		// register the filter
		LocalBroadcastManager.getInstance(this).registerReceiver(
				onReceiveBroadcast, filter);
	}

	/**
	 * Broadcast receivers
	 */
	private BroadcastReceiver onReceiveBroadcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "In BroadcastReceiver.onReceive");

			// extract the response extras
			String responseStatus = intent
					.getStringExtra(SimpleAppConstants.SERVICE_STATUS);
			String responseStatusInfo = intent
					.getStringExtra(SimpleAppConstants.SERVICE_STATUS_INFO);
			String responseAction = intent.getAction();

			if (M3MessageConstants.STATUS_SUCCESS
					.equalsIgnoreCase(responseStatus)) {

				Log.d(TAG, "ResponseAction = " + responseAction);
				Log.d(TAG, "ResponseStatus = " + responseStatus);
				Log.d(TAG, "ResponseStatusInfo = " + responseStatusInfo);
				
				if (BrandService.ACTION_GET_BRANDS.equalsIgnoreCase(responseAction)) {

					BrandWrapper brands = intent
							.getParcelableExtra(SimpleAppConstants.SERVICE_RESULT);

					receiveGetBrands(brands);
					
				} else if (BrandService.ACTION_FIND_BRAND.equalsIgnoreCase(responseAction)) {
					
				}

			} else {
				// FAIL: log the issue
				Log.e(TAG, "Error processing service: " + responseStatus + ": "
						+ responseStatusInfo);
			}

		}

		private void receiveGetBrands(BrandWrapper brandWrapper) {

			if (brandWrapper != null) {
				Log.d(TAG, "brandWrapper.brands.size = " + brandWrapper.getBrands().size());
//				for (Brand b : brands.getBrands()) {
//					Log.d(TAG, b.toString());
//				}
				
				Intent intent = new Intent(HomeActivity.this, BrandActivity.class);
				// Strip off the wrapper class and just pass the list of brands.
				intent.putParcelableArrayListExtra(BrandActivity.BRANDS, brandWrapper.getBrands());
				startActivity(intent);
				
			} else {
				Log.w(TAG, "Brands is null.");
			}
			
			
		}
	};
}
