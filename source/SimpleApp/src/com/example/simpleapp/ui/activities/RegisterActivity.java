package com.example.simpleapp.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.M3MessageConstants;
import com.example.simpleapp.R;
import com.example.simpleapp.services.IdentityService;
import com.example.simpleapp.ui.fragments.RegisterFragment;
import com.example.simpleapp.util.SimpleAppConstants;

public class RegisterActivity extends BaseActivity {

	public static final String TAG = RegisterActivity.class.getName();

	protected RegisterFragment fragment;

	public RegisterActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		if (savedInstanceState == null) {
			fragment = new RegisterFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void register(View view) {
		Log.d(TAG, "User clicked the register button.");

		EditText nameEditText = (EditText) this.findViewById(R.id.nameText);
		EditText emailEditText = (EditText) this.findViewById(R.id.emailText);
		EditText pwdEditText = (EditText) this.findViewById(R.id.passwordText);
		EditText deviceEditText = (EditText) this.findViewById(R.id.deviceText);
		EditText randEditText = (EditText) this.findViewById(R.id.randText);

		String name = nameEditText.getText().toString();
		String email = emailEditText.getText().toString();
		String pwd = pwdEditText.getText().toString();
		String device = deviceEditText.getText().toString();
		String rand = randEditText.getText().toString();

		// Create the device tag.
		String deviceTag = email + "|" + device + "|" + rand;

		String tmpDeviceToken = SimpleAppConstants.TMP_DEVICE_TOKEN;

		// Save the registration info as preferences.
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		prefs.edit().putString(SimpleAppConstants.PREF_NAME_KEY, name).apply();
		prefs.edit().putString(SimpleAppConstants.PREF_EMAIL_KEY, email)
				.apply();
		prefs.edit().putString(SimpleAppConstants.PREF_DEVICE_KEY, device)
				.apply();
		prefs.edit()
				.putString(SimpleAppConstants.PREF_DEVICE_TAG_KEY, deviceTag)
				.apply();
		prefs.edit()
				.putString(SimpleAppConstants.PREF_DEVICE_TOKEN_KEY,
						tmpDeviceToken).apply();

		// For now, try the user login and then the device login to validate the
		// data.
		// The password is passed in because we won't store the password on the
		// device.
		// We just want it at registration to confirm that the user is
		// registered.
		validateLogin(pwd);

		// registerUser(name,email,pwd);
		// registerDevice(email, device, rand);

	}

	private void validateLogin(String password) {
		Log.d(TAG, "In validateLogin()");

		Intent intent = new Intent(RegisterActivity.this, IdentityService.class);
		intent.putExtra(IdentityService.EXTRA_PASSWORD, password);
		intent.setAction(IdentityService.ACTION_VALIDATE_LOGIN);
		startService(intent);

	}

	protected void registerUser(String fullName, String email, String password) {

		Log.d(TAG, "In registerUser()");
	}

	protected void registerDevice(String email, String device, String rand) {

		Log.d(TAG, "In registerDevice()");
	}

	/**
	 * Broadcast receivers
	 */
	private BroadcastReceiver onReceiveBroadcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "In BroadcastReceiver.onReceive");

			// extract the response extras
			String responseStatus = intent
					.getStringExtra(SimpleAppConstants.SERVICE_STATUS);
			String responseStatusInfo = intent
					.getStringExtra(SimpleAppConstants.SERVICE_STATUS_INFO);
			String responseAction = intent.getAction();

			if (M3MessageConstants.STATUS_SUCCESS
					.equalsIgnoreCase(responseStatus)) {

				Log.d(TAG, "ResponseAction = " + responseAction);
				Log.d(TAG, "ResponseStatus = " + responseStatus);
				Log.d(TAG, "ResponseStatusInfo = " + responseStatusInfo);

				if (IdentityService.ACTION_VALIDATE_LOGIN
						.equalsIgnoreCase(responseAction)) {

					Intent nextIntent = new Intent(RegisterActivity.this,
							HomeActivity.class);
					startActivity(nextIntent);

				}

			} else {
				// FAIL: log the issue
				Log.e(TAG, "Error processing service: " + responseStatus + ": "
						+ responseStatusInfo);
			}

		}
	};

}
