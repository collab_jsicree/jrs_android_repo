package com.example.simpleapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.simpleapp.R;
import com.example.simpleapp.domain.Brands;
import com.example.simpleapp.ui.fragments.BrandFragment;

public class BrandActivity extends BaseActivity {

	public static final String TAG = "BrandActivity";
	protected static final String BRANDS = "BRANDS";

	public BrandActivity() {
		super();
	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_brand);

		Brands b = this.getIntent().getParcelableExtra(BRANDS);
		if (b != null) {
			Log.d(TAG,"brands.size = " + b.size());			
		}
		
		if (savedInstanceState == null) {
			fragment = new BrandFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();

		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.brand, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			showSettings();
		}
		return super.onOptionsItemSelected(item);
	}

	protected void showSettings() {
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);
	}

}
