package com.example.simpleapp.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.collaborative.m3.clientsdk.serviceapi.servicemessage.M3MessageConstants;
import com.example.simpleapp.R;
import com.example.simpleapp.services.IdentityService;
import com.example.simpleapp.ui.fragments.SplashFragment;
import com.example.simpleapp.util.SimpleAppConstants;

public class SplashActivity extends BaseActivity {

	public static final String TAG = "SplashActivity";

	public SplashActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		if (savedInstanceState == null) {
			fragment = new SplashFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();

		}

		loginDevice();
	}

	public void loginDevice() {
		Log.d(TAG, "In loginDevice()");

		Intent intent = new Intent(SplashActivity.this, IdentityService.class);
		intent.setAction(IdentityService.ACTION_LOGIN_DEVICE);
		startService(intent);

	}

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				onReceiveBroadcast);

	}

	@Override
	protected void onResume() {
		super.onResume();

		// create the intent filter
		IntentFilter filter = new IntentFilter();
		filter.addAction(IdentityService.ACTION_LOGIN_DEVICE);

		// register the filter
		LocalBroadcastManager.getInstance(this).registerReceiver(
				onReceiveBroadcast, filter);
	}

	/**
	 * Broadcast receivers
	 */
	private BroadcastReceiver onReceiveBroadcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "In BroadcastReceiver.onReceive");

			// extract the response extras
			String responseStatus = intent
					.getStringExtra(SimpleAppConstants.SERVICE_STATUS);
			String responseStatusInfo = intent
					.getStringExtra(SimpleAppConstants.SERVICE_STATUS_INFO);
			String responseAction = intent.getAction();

			Log.d(TAG, "ResponseAction = " + responseAction);
			Log.d(TAG, "ResponseStatus = " + responseStatus);
			Log.d(TAG, "ResponseStatusInfo = " + responseStatusInfo);

			if (IdentityService.ACTION_LOGIN_DEVICE
					.equalsIgnoreCase(responseAction)) {
				if (M3MessageConstants.STATUS_SUCCESS
						.equalsIgnoreCase(responseStatus)) {

					Intent nextIntent = new Intent(SplashActivity.this,
							HomeActivity.class);
					startActivity(nextIntent);

				} else {

					Intent nextIntent = new Intent(SplashActivity.this,
							RegisterActivity.class);
					startActivity(nextIntent);
				}
			}
		}
	};
}
