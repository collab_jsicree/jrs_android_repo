package com.example.simpleapp.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simpleapp.R;
import com.example.simpleapp.ui.activities.RegisterActivity;
import com.example.simpleapp.util.SimpleAppConstants;

public class BrandFragment extends BaseFragment {

	public static final String TAG = BrandFragment.class.getName();

	public BrandFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,"In onCreateView.");

		View rootView = inflater.inflate(R.layout.fragment_brand, container,
				false);
		return rootView;
	}

}
