package com.example.simpleapp.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simpleapp.R;

public class RegisterFragment extends BaseFragment {

	public static final String TAG = RegisterFragment.class.getName();;

	public RegisterFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,"In onCreateView.");
		
		View rootView = inflater.inflate(R.layout.fragment_register, container,
				false);
		return rootView;
	}
	
}
