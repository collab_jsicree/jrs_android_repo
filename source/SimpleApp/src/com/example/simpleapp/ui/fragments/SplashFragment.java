package com.example.simpleapp.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simpleapp.R;

public class SplashFragment extends BaseFragment {

	public static final String TAG = "SplashFragment";

	public SplashFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,"In onCreateView.");

		View rootView = inflater.inflate(R.layout.fragment_splash, container,
				false);
		return rootView;
	}

}
