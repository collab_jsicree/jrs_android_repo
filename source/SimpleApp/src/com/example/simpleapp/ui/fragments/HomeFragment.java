package com.example.simpleapp.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simpleapp.R;
import com.example.simpleapp.ui.activities.RegisterActivity;
import com.example.simpleapp.util.SimpleAppConstants;

public class HomeFragment extends BaseFragment {

	public static final String TAG = HomeFragment.class.getName();

	public HomeFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,"In onCreateView.");

//		SharedPreferences prefs = PreferenceManager
//				.getDefaultSharedPreferences(this.getActivity());
//		String deviceTag = prefs.getString(SimpleAppConstants.DEVICE_TAG, "");
//		String deviceToken = prefs.getString(SimpleAppConstants.DEVICE_TOKEN,
//				"");
//
//		Log.d(TAG, "DeviceTag = " + deviceTag);
//		Log.d(TAG, "DeviceToken = " + deviceToken);
//
//		if (deviceTag.isEmpty() || deviceToken.isEmpty()) {
//			Log.d(TAG, "No tag or token. Redirecting to the RegisterActivity.");
//			Intent intent = new Intent(this.getActivity(), RegisterActivity.class);
////			String message = editText.getText().toString();
////			intent.putExtra(, message);
//			this.getActivity().startActivity(intent);
//		} else {
//			Log.d(TAG, "Tag and token found.");
//		}
		
		View rootView = inflater.inflate(R.layout.fragment_home, container,
				false);
		return rootView;
	}

}
