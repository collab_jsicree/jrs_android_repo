package com.example.simpleapp.ui.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.example.simpleapp.R;
import com.example.simpleapp.ui.activities.RegisterActivity;
import com.example.simpleapp.util.SimpleAppConstants;

public class SettingsFragment extends PreferenceFragment {

	public static final String TAG = SettingsFragment.class.getName();;

	public SettingsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);

	}	

}
