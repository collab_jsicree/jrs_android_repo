package com.example.simpleapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.collaborative.m3.clientsdk.context.M3AppContext;
import com.collaborative.m3.clientsdk.serviceapi.M3ServiceClient;
import com.collaborative.m3.clientsdk.serviceapi.servicemessage.M3MessageConstants;
import com.example.simpleapp.messages.LoginDeviceParams;
import com.example.simpleapp.messages.LoginDeviceRequest;
import com.example.simpleapp.messages.LoginDeviceResponse;
import com.example.simpleapp.messages.LoginUserParams;
import com.example.simpleapp.messages.LoginUserRequest;
import com.example.simpleapp.messages.LoginUserResponse;
import com.example.simpleapp.util.SimpleAppConstants;

public class IdentityService extends IntentService {

	public static final String INTENT_SERVICE_NAME = "com.example.simpleapp.services.IdentityService";
	public static final String TAG = "IdentityService";
	public static final String ACTION_LOGIN_USER = "com.example.simpleapp.services.IdentityService.ACTION_LOGIN_USER";
	public static final String ACTION_REGISTER_DEVICE = "com.example.simpleapp.services.IdentityService.ACTION_REGISTER_DEVICE";
	public static final String ACTION_LOGIN_DEVICE = "com.example.simpleapp.services.IdentityService.ACTION_LOGIN_DEVICE";
	public static final String ACTION_LOGOUT = "com.example.simpleapp.services.IdentityService.ACTION_LOGOUT";
	public static final String ACTION_VALIDATE_LOGIN = "com.example.simpleapp.services.IdentityService.ACTION_VALIDATE_LOGIN";
	public static final String EXTRA_PASSWORD = "PASSWORD";

	public IdentityService() {
		super(INTENT_SERVICE_NAME);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "In onHandleIntent()");

		// initialize M3
		M3AppContext.init(this.getApplicationContext());

		// get the list action
		String action = intent.getAction();

		// check to make sure action was set
		if (action == null) {
			Log.e(IdentityService.INTENT_SERVICE_NAME,
					"Action not provided by INTENT sender.");
			return;
		}

		// route the request
		if (action.equalsIgnoreCase(IdentityService.ACTION_LOGIN_USER)) {
			this.handleLoginUser(intent);

		} else if (action
				.equalsIgnoreCase(IdentityService.ACTION_REGISTER_DEVICE)) {
			this.handleRegisterDevice(intent);

		} else if (action.equalsIgnoreCase(IdentityService.ACTION_LOGIN_DEVICE)) {
			this.handleLoginDevice(intent);

		} else if (action.equalsIgnoreCase(IdentityService.ACTION_LOGOUT)) {
			this.handleLogout(intent);

		} else if (action
				.equalsIgnoreCase(IdentityService.ACTION_VALIDATE_LOGIN)) {
			this.handleValidateLogin(intent);

		} else {
			Log.e(TAG, "Action " + action + " not supported.");
		}
	}

	private void handleLoginUser(Intent intent) {
		Log.d(TAG, "In handleLoginUser()");

		final Intent responseIntent = new Intent(
				IdentityService.ACTION_LOGIN_USER);

		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS, "OK");
		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS_INFO,
				"sample status info");

		// responseIntent.putExtra(SimpleAppConstants.SERVICE_RESULT,
		// response.getBody());

		// send the message
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(responseIntent);

	}

	private void handleRegisterDevice(Intent intent) {
		Log.d(TAG, "In handleRegisterDevice()");

		final Intent responseIntent = new Intent(
				IdentityService.ACTION_REGISTER_DEVICE);

		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS, "OK");
		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS_INFO,
				"sample status info");

		// responseIntent.putExtra(SimpleAppConstants.SERVICE_RESULT,
		// response.getBody());

		// send the message
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(responseIntent);

	}

	private void handleLoginDevice(Intent intent) {
		Log.d(TAG, "In handleLoginDevice()");

		String statusCode = doDeviceLogin();

		if (M3MessageConstants.STATUS_SUCCESS.equalsIgnoreCase(statusCode)) {
			Log.d(TAG, "Device login successful.");
		} else {
			Log.w(TAG, "Device login failed.");
		}

		final Intent responseIntent = new Intent(
				IdentityService.ACTION_LOGIN_DEVICE);

		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS, statusCode);

		// send the message
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(responseIntent);

	}

	private void handleLogout(Intent intent) {
		Log.d(TAG, "In handleLogout()");

		final Intent responseIntent = new Intent(IdentityService.ACTION_LOGOUT);

		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS, "OK");
		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS_INFO,
				"sample status info");

		// responseIntent.putExtra(SimpleAppConstants.SERVICE_RESULT,
		// response.getBody());

		// send the message
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(responseIntent);

	}

	private void handleValidateLogin(Intent intent) {
		Log.d(TAG, "In handleValidateLogin()");

		// We don't store the password in preferences so we sent it in as an
		// extra
		String password = intent.getStringExtra(IdentityService.EXTRA_PASSWORD);

		if (M3MessageConstants.STATUS_SUCCESS
				.equalsIgnoreCase(doUserLogin(password))) {
			Log.d(TAG, "User login successful.");
			if (M3MessageConstants.STATUS_SUCCESS
					.equalsIgnoreCase(doDeviceLogin())) {
				Log.d(TAG, "Device login successful.");
			} else {
				Log.w(TAG, "Device login failed.");
			}
		} else {
			Log.w(TAG, "User login failed.");
		}

		final Intent responseIntent = new Intent(
				IdentityService.ACTION_VALIDATE_LOGIN);

		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS, "OK");
		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS_INFO,
				"sample status info");

		// send the message
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(responseIntent);

	}

	protected String doUserLogin(String password) {
		Log.d(TAG, "In doUserLogin()");

		String status = M3MessageConstants.STATUS_FAIL_UNKNOWN;

		LoginUserParams loginParams = new LoginUserParams();

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		String email = prefs.getString(SimpleAppConstants.PREF_EMAIL_KEY, null);

		loginParams.setUserName(email);
		loginParams.setPassword(password);
		loginParams.setRememberMe(false);

		// create the Request and set required parameters
		LoginUserRequest request = new LoginUserRequest();
		request.setUrl(SimpleAppConstants.SERVICE_BASE_URL
				+ SimpleAppConstants.SERVICE_USER_LOGIN);
		request.setResponseBodyType(LoginUserResponse.class);

		// set the body to the request message
		request.setBody(loginParams);

		// declare the response
		LoginUserResponse loginResponse = null;

		// invoke the service
		loginResponse = (LoginUserResponse) M3ServiceClient
				.invokeServicePOST(request);

		if (loginResponse != null) {
			status = loginResponse.getM3Header().getStatus().getStatusCode();

			Log.d(TAG, "Status = "
					+ loginResponse.getM3Header().getStatus().getStatusCode());
			Log.d(TAG, "StatusInfo = "
					+ loginResponse.getM3Header().getStatus().getStatusInfo());
		}

		return status;
	}

	protected String doDeviceLogin() {
		Log.d(TAG, "In doDeviceLogin()");
		String status = M3MessageConstants.STATUS_FAIL_UNKNOWN;

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		String tag = prefs.getString(SimpleAppConstants.PREF_DEVICE_TAG_KEY,
				null);
		String token = prefs.getString(
				SimpleAppConstants.PREF_DEVICE_TOKEN_KEY, null);

		LoginDeviceParams loginParams = new LoginDeviceParams();
		loginParams.setDeviceAppRegistrationTag(tag);
		loginParams.setDeviceToken(token);

		// create the Request and set required parameters
		LoginDeviceRequest request = new LoginDeviceRequest();
		request.setUrl(SimpleAppConstants.SERVICE_BASE_URL
				+ SimpleAppConstants.SERVICE_DEVICE_LOGIN);
		request.setResponseBodyType(LoginDeviceResponse.class);

		// set the body to the request message
		request.setBody(loginParams);

		// declare the response
		LoginDeviceResponse loginResponse = null;

		// invoke the service
		loginResponse = (LoginDeviceResponse) M3ServiceClient
				.invokeServicePOST(request);

		if (loginResponse != null) {
			status = loginResponse.getM3Header().getStatus().getStatusCode();
			Log.d(TAG, "Status = "
					+ loginResponse.getM3Header().getStatus().getStatusCode());
			Log.d(TAG, "StatusInfo = "
					+ loginResponse.getM3Header().getStatus().getStatusInfo());
		}

		return status;
	}

}
