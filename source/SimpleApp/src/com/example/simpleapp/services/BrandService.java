package com.example.simpleapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.collaborative.m3.clientsdk.context.M3AppContext;
import com.collaborative.m3.clientsdk.serviceapi.M3ServiceClient;
import com.collaborative.m3.clientsdk.serviceapi.servicemessage.M3SimpleRequestMessage;
import com.example.simpleapp.messages.BrandResponse;
import com.example.simpleapp.util.SimpleAppConstants;

public class BrandService extends IntentService {

	public static final String INTENT_SERVICE_NAME = "com.example.simpleapp.services.BrandService";
	public static final String TAG = "BrandService";
	public static final String ACTION_GET_BRANDS = "com.example.simpleapp.services.BrandService.ACTION_GET_BRANDS";
	public static final String ACTION_FIND_BRAND = "com.example.simpleapp.services.BrandService.ACTION_FIND_BRAND";

	public BrandService() {
		super(INTENT_SERVICE_NAME);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "In onHandleIntent()");

		// initialize M3
		M3AppContext.init(this.getApplicationContext());

		// get the list action
		String action = intent.getAction();

		// check to make sure action was set
		if (action == null) {
			Log.e(BrandService.INTENT_SERVICE_NAME,
					"Action not provided by INTENT sender.");
			return;
		}

		// route the request
		if (action.equalsIgnoreCase(BrandService.ACTION_GET_BRANDS)) {
			this.handleGetBrands(intent);

		} else if (action.equalsIgnoreCase(BrandService.ACTION_FIND_BRAND)) {
			this.handleFindBrand(intent);

		} else {
			Log.e(TAG, "Action " + action + " not supported.");
		}
	}

	private void handleFindBrand(Intent intent) {
		Log.d(TAG, "In handleFindBrand()");
	}

	private void handleGetBrands(Intent intent) {
		Log.d(TAG, "In handleGetBrands()");

		// Create the M3 request message
		M3SimpleRequestMessage m3RequestMsg = new M3SimpleRequestMessage();

		// set request parameters
		m3RequestMsg.setUrl(SimpleAppConstants.SERVICE_BASE_URL
				+ SimpleAppConstants.SERVICE_GET_BRANDS);
		m3RequestMsg.setResponseBodyType(BrandResponse.class);

		// declare the response
		BrandResponse brandResponse = null;

		// invoke the service
		brandResponse = (BrandResponse) M3ServiceClient
				.invokeServiceGET(m3RequestMsg);
		if (brandResponse != null && brandResponse.getBody() != null && brandResponse.getBody().getBrands() != null) {
			Log.d(TAG, "BrandResponse.getBody.getBrandList.size = " + brandResponse.getBody().getBrands().size());			
		}
		final Intent responseIntent = new Intent(BrandService.ACTION_GET_BRANDS);

		// responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS, "OK");
		// responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS_INFO,
		// "sample status info");

		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS,
				brandResponse.getM3Header().getStatus().getStatusCode());
		responseIntent.putExtra(SimpleAppConstants.SERVICE_STATUS_INFO,
				brandResponse.getM3Header().getStatus().getStatusInfo());
		responseIntent.putExtra(SimpleAppConstants.SERVICE_RESULT, brandResponse.getBody());
		// send the message
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(responseIntent);

	}

}
